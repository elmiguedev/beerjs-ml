const SpotifyWebApi = require('spotify-web-api-node');


module.exports = {

    spotifyApi: new SpotifyWebApi({
        clientId: '9eba6ea040854f0da7c42b6981082376',
        clientSecret: 'a12acf97edf84ae7824e04dee2501b43',
    }),

    async connect() {

        const data = await this.spotifyApi.clientCredentialsGrant();
        this.spotifyApi.setAccessToken(data.body['access_token']);
        console.log('spoti connected :D');


    },

    testMethod() {
        this.spotifyApi.getPlaylist('5ieJqeLJjjI8iJWaxeBLuK')
        .then(function(data) {
          console.log('Some information about this playlist', data.body);
        }, function(err) {
          console.log('Something went wrong!', err);
        });
    },

    async getSongs(playlistId) {
        // get complete tracklist
        const tks = await this.spotifyApi.getPlaylistTracks(playlistId, {
            offset: 1,
            limit: 50,
            fields: 'items'
        });

        // get just simple data
        const songs = await tks.body.items.map(x => {
            return {
                id: x.track.id,
                name: x.track.name,
                artist: x.track.artists[0].name,
            };
        });

        for (const song of songs) {
            const features = await this.getFeatures(song.id);
            song.features = features;
        }

        return songs;
    },

    async getFeatures(trackId) {
        const f = await this.spotifyApi.getAudioFeaturesForTrack(trackId);

        let features = [];
        features.push(f.body.danceability);
        features.push(f.body.energy);
        features.push(f.body.speechiness);
        features.push(f.body.acousticness);
        features.push(f.body.instrumentalness);
        features.push(f.body.liveness);
        features.push(f.body.valence);

        return features;
    }

}
